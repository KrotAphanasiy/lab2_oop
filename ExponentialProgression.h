//
// Created by michael on 15.09.2020.
//

#ifndef LAB2_OOP_EXPONENTIALPROGRESSION_H
#define LAB2_OOP_EXPONENTIALPROGRESSION_H
#include "Progression.h"

class ExponentialProgression : public Progression{
public:
    ExponentialProgression() = default;
    ExponentialProgression(double *progressionMembs, int elementsAmount);
    ExponentialProgression(double firstElement, int elementsAmount, double feature);
    ExponentialProgression(double firstElement, double secondElement, int elementsAmount);

    double CalculateSum() override;
    double CalculateNElement(int elNumber) override;
    double CalculateFeature() override;
    double CalculateAbsoluteSum() override;

protected:
    double _secondElement = 0;
    double _infDecrSum = 0;
    bool _infDecrSumCalculated = false;
};


#endif LAB2_OOP_EXPONENTIALPROGRESSION_H
