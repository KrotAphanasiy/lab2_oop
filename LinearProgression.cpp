//
// Created by michael on 15.09.2020.
//

#include "LinearProgression.h"

LinearProgression::LinearProgression(double *progressionMembs, int elementsAmount) :
        Progression(progressionMembs, elementsAmount),
        _lastElement(progressionMembs[_elementsAmount - 1]){

    double prev = _progressionMembs[1] - _progressionMembs[0];

    for (int counter = 2; counter < _elementsAmount; counter++){
        if(!DbsAreSame(_progressionMembs[counter] - _progressionMembs[counter - 1], prev)){
            CleanUp();
            throw ProgressionExc("Sequence is not progression");
        }
    }
}

LinearProgression::LinearProgression(double firstElement, int elementsAmount, double feature) :
        Progression(firstElement, elementsAmount, feature){
}

LinearProgression::LinearProgression(double firstElement, double lastElement, int elementsAmount):
        Progression(firstElement, elementsAmount),
        _lastElement(lastElement){

}

double LinearProgression::CalculateSum() {
    if (!_featurePassed) {
        _sum = (_firstElement + _lastElement) / 2;
        _sum = _sum * _elementsAmount;
    } else {
        _sum = _firstElement * 2 + _feature * (_elementsAmount - 1);
        _sum = _sum / 2;
        _sum = _sum * _elementsAmount;
    }
    _sumCalculated = true;
    return _sum;
}

double LinearProgression::CalculateNElement(int elNumber) {
    CalculateFeature();
    return _firstElement + _feature * (elNumber - 1);
}

double LinearProgression::CalculateFeature() {
    if(_featurePassed) {
        return _feature;
    }else if(_progressionMembs != nullptr) {
        _feature = _progressionMembs[1] - _progressionMembs[0];
        return _feature;
    }else if(_sumCalculated){
        _feature = (_lastElement - _firstElement) / (_elementsAmount - 1);
        return _feature;
    }else{
        throw ProgressionExc("can`t calculate Linear feature before sum");
    }
}

double LinearProgression::CalculateAbsoluteSum() {
    throw ProgressionExc("Can`t calculate for this progression");
}
