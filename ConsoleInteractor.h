//
// Created by michael on 15.09.2020.
//

#ifndef LAB2_OOP_CONSOLEINTERACTOR_H
#define LAB2_OOP_CONSOLEINTERACTOR_H
#include "Progression.h"
#include "LinearProgression.h"
#include "ExponentialProgression.h"
#include "ConsoleInteractorExc.h"

class ConsoleInteractor {
public:
    ConsoleInteractor() = default;
    ~ConsoleInteractor();

    void init();

private:
    Progression* _progression = nullptr;
    std::string _progressionType;
    int _progressionsAmount = 0;
    void ProcessProgression();
    void ReadProgressionFromFile(std::istream& fin);
    void ShowProgression(std::ostream& out);
    static double* ReadSequence(int elementsAmount, std::istream& fin);
    void CreateProgression(int param);
    void CleanUp();
};


#endif LAB2_OOP_CONSOLEINTERACTOR_H
