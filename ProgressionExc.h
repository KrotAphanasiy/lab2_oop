//
// Created by michael on 15.09.2020.
//

#ifndef LAB2_OOP_PROGRESSIONEXC_H
#define LAB2_OOP_PROGRESSIONEXC_H

#include <string>

class ProgressionExc: public std::exception{
public:
    explicit ProgressionExc(const std::string& error): _error(error){}
    const char* what() const noexcept override{
        return _error.c_str();
    }
private:
    std::string _error;
};

#endif LAB2_OOP_PROGRESSIONEXC_H
