//
// Created by michael on 15.09.2020.
//

#include "Progression.h"
#include <ostream>
#include <cmath>

bool Progression::DbsAreSame(double a, double b)
{
    return fabs(a - b) < 1e-24;
}


Progression::Progression(double *progressionMembs, int elementsAmount):
    _progressionMembs(progressionMembs),
    _elementsAmount(elementsAmount){
    if(_progressionMembs != nullptr && _elementsAmount > 1){
        _firstElement = _progressionMembs[0];
    }else{
        CleanUp();
        throw ProgressionExc("Progression is empty or too short");
    }
}


Progression::Progression(double firstElement, int elementsAmount, double feature):
    _firstElement(firstElement),
    _elementsAmount(elementsAmount),
    _feature(feature){

    if(_feature != 0){
        _featurePassed = true;
    }

}

double Progression::GetSum() const {
    return _sum;
}

double Progression::GetFeature() const {
    return _feature;
}


double Progression::GetElementsAmount() const {
    return _elementsAmount;
}

void Progression::CleanUp() {
    if(_progressionMembs != nullptr){
        delete[] _progressionMembs;
        _progressionMembs = nullptr;
    }
}

std::ostream& operator<<(std::ostream &out, const Progression& progression) {
    if(progression._progressionMembs != nullptr){
        for(int counter = 0; counter < progression._elementsAmount; counter++){
            out << progression._progressionMembs[counter];
            if(counter < progression._elementsAmount - 1){
                out << " ";
            }
        }
        return out;
    }else{
        throw ProgressionExc("no progressionMembs to flush");
    }
}

Progression::~Progression() {
    CleanUp();
}

bool Progression::GetFeatureStatus() const {
    return _featurePassed;
}


