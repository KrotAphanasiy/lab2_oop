//
// Created by michael on 15.09.2020.
//

#ifndef LAB2_OOP_LINEARPROGRESSION_H
#define LAB2_OOP_LINEARPROGRESSION_H
#include "Progression.h"

class LinearProgression: public Progression {
public:
    LinearProgression() = default;
    LinearProgression(double *progressionMembs, int elementsAmount);
    LinearProgression(double firstElement, int elementsAmount, double feature);
    LinearProgression(double firstElement, double lastElement, int elementsAmount);

    double CalculateSum() override;
    double CalculateNElement(int elNumber) override;
    double CalculateFeature() override;
    double CalculateAbsoluteSum() override;

protected:
    double _lastElement = 0;
};


#endif LAB2_OOP_LINEARPROGRESSION_H
