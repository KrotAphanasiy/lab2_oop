//
// Created by michael on 15.09.2020.
//

#include "ConsoleInteractor.h"
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::cin;
using std::ifstream;
using std::string;


void ConsoleInteractor::init() {

    int nextAction = -1;
    cout << "Как ввести прогрессию? :\n1 - из файла - автоматический рассчет параметров (можно несколько)\n"
            "2 - вручную - просчет параметров ручной, одновременно - одна прогрессия\n\n";

    while (true) {

        cout << ">> ";
        cin >> nextAction;

        if (nextAction == 1) {
            string fileName;
            cout << "Введите имя файла: ";
            cin >> fileName;
            ifstream fin(fileName, std::ios::in);
            if (!fin.is_open()) {
                CleanUp();
                throw ConsoleInteractorExc("file doesn`t exist");
            }
            ifstream &finRef = fin;
            fin >> _progressionsAmount;
            int counter = 0;
            while (!fin.eof() && counter < _progressionsAmount) {
                try {
                    CleanUp();
                    ReadProgressionFromFile(finRef);
                    ProcessProgression();
                    ShowProgression(cout);
                    _progressionType = "";
                    counter++;
                }catch(const ProgressionExc& exc){
                    std::cerr << exc.what();
                }
            }

        } else if (nextAction == 2) {
            cout << "Тип прогрессии (1 - арифметическая, 2 - геометрическая) и способ инициализации\n"
                    "Общие:\n(1 - количество членов и сами члены, 2 - первый элемент, количесвто элементов, характеристика)\n"
                    "Только для арифметической:\n(3 - первый и последний элементы, количесвто элементов)\n"
                    "Только для геометрической:\n(4 - первый и второй членыб количество элементов)\n"
                    "Пример команды >> 12\n";

            cout << ">> ";
            cin >> nextAction;

            try {
                if(nextAction == 0){
                    continue;
                }
                CreateProgression(nextAction);
            }catch(const ProgressionExc& exc){
                std::cerr << exc.what();
                //CleanUp();
                return;
            }

            cout << "\nЧто вы хотите сделать?\n"
                    "1 - Вычислить сумму прогрессии\n"
                    "2 - Вычислить н-ый элемент\n"
                    "3 - Вычислить дельту\n"
                    "0 - Выйти в главное меню\n";
            if(_progressionType == "Exponential" && _progression->GetFeatureStatus() && std::abs(_progression->GetFeature()) < 1){
                cout << "4 - Вычислить сумму бесконечно убывающей геом. прогрессии\n";
            }
            cout << "\n";

            while(true){
                cout << ">> ";
                cin >> nextAction;
                if(nextAction == 0){
                    break;
                }else if(nextAction == 1){
                    cout << "Сумма: " << _progression->CalculateSum() << std::endl;
                }else if(nextAction == 2){
                    cout << "Введите номер элемента: ";
                    int num;
                    cin >> num;
                    cout << "Элемент " << num << " равен " << _progression->CalculateNElement(num) << std::endl;
                }else if(nextAction == 3){
                    cout << "Дельта равна " << _progression->CalculateFeature() << std::endl;
                }else if(nextAction == 4 && _progressionType == "Exponential" && _progression->GetFeatureStatus()){
                    cout << "Сумма беск. убыв. равна " << _progression->CalculateAbsoluteSum() << std::endl;
                }
            }

        }else if (nextAction == 0){
            return;
        }
    }
}

void ConsoleInteractor::ProcessProgression() {
    _progression->CalculateFeature();
    _progression->CalculateSum();
    if(_progressionType == "Exponential" && std::abs(_progression->GetFeature()) < 1){
        _progression->CalculateAbsoluteSum();
    }
}

void ConsoleInteractor::ReadProgressionFromFile(std::istream &fin) {
    int elementsAmount = 0;

    while(_progressionType.empty()) {
        fin >> _progressionType;
        try {
            if (_progressionType == "Linear") {
                fin >> elementsAmount;
                _progression = new LinearProgression(ReadSequence(elementsAmount, fin),
                                                     elementsAmount);
            } else if (_progressionType == "Exponential") {
                fin >> elementsAmount;
                _progression = new ExponentialProgression(ReadSequence(elementsAmount, fin),
                                                          elementsAmount);
            }
        }catch(const ProgressionExc& exc){
            throw exc;
        }
    }
}

void ConsoleInteractor::ShowProgression(std::ostream &out) {
    out << _progressionType << std::endl;
    out << "Members: " << *_progression << std::endl;
    out << "Sum: " << _progression->GetSum() << std::endl;
    out << "Feature: " << _progression->GetFeature() << std::endl;
    if(_progressionType == "Exponential" && std::abs(_progression->GetFeature()) < 1) {
        out << "Absolute sum: " << _progression->CalculateAbsoluteSum() << std::endl;
    }
    out << std::endl;
}

double *ConsoleInteractor::ReadSequence(int elementsAmount, std::istream &fin) {
    double* sequence = new double[elementsAmount];
    for(int counter = 0; counter < elementsAmount; counter++){
        fin >> sequence[counter];
    }
    return sequence;
}

void ConsoleInteractor::CleanUp() {
    _progressionType = "";
    delete _progression;
}

ConsoleInteractor::~ConsoleInteractor() {
    CleanUp();
}

void ConsoleInteractor::CreateProgression(int param) {
    double first, last;
    int elsAmount;
    double feature;
    CleanUp();
    try {
        if (param == 11) {
            cout << "Количество элементов: ";
            cin >> elsAmount;
            cout << "Члены: ";
            _progression = new LinearProgression(ReadSequence(elsAmount, cin), elsAmount);
            _progressionType = "Linear";
        } else if (param == 12) {
            cout << "Первый элемент, количество элементов, характеристика: ";
            cin >> first >> elsAmount >> feature;
            _progression = new LinearProgression(first, elsAmount, feature);
            _progressionType = "Linear";
        } else if (param == 13) {
            cout << "Первый элемент, последний элемент, количество элементов: ";
            cin >> first >> last >> elsAmount;
            _progression = new LinearProgression(first, last, elsAmount);
            _progressionType = "Linear";
        } else if (param == 21) {
            cout << "Количество элементов: ";
            cin >> elsAmount;
            cout << "Члены: ";
            _progression = new ExponentialProgression(ReadSequence(elsAmount, cin), elsAmount);
            _progressionType = "Exponential";
        } else if (param == 22) {
            cout << "Первый элемент, количество элементов, характеристика: ";
            cin >> first >> elsAmount >> feature;
            _progression = new ExponentialProgression(first, elsAmount, feature);
            _progressionType = "Exponential";
        } else if (param == 23) {
            cout << "Первый элемент, второй элемент, количество элементов: ";
            cin >> first >> last >> elsAmount;
            _progression = new ExponentialProgression(first, last, elsAmount);
            _progressionType = "Exponential";
        }
    }catch(const ProgressionExc& exc){
        throw exc;
    }

}



