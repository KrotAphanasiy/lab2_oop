//
// Created by michael on 17.09.2020.
//

#ifndef LAB2_OOP_CONSOLEINTERACTOREXC_H
#define LAB2_OOP_CONSOLEINTERACTOREXC_H

#include <string>

class ConsoleInteractorExc: public std::exception{
public:
    ConsoleInteractorExc(const std::string& error): _error(error){}
    const char* what() const noexcept override{
        return _error.c_str();
    }
private:
    std::string _error;
};

#endif LAB2_OOP_CONSOLEINTERACTOREXC_H
