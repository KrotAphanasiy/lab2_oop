//
// Created by michael on 15.09.2020.
//

#ifndef LAB2_OOP_PROGRESSION_H
#define LAB2_OOP_PROGRESSION_H
#include "ProgressionExc.h"

class Progression {
public:
    Progression() = default;
    Progression(double *progressionMembs, int elementsAmount);
    explicit Progression(double firstElement, int elementsAmount = 0, double feature = 0);

    virtual double CalculateSum() = 0;
    virtual double CalculateNElement(int elNumber) = 0;
    virtual double CalculateFeature() = 0;
    virtual double CalculateAbsoluteSum() = 0;

    double GetSum() const;
    double GetFeature() const;
    bool GetFeatureStatus() const;
    double GetElementsAmount() const;


    friend std::ostream& operator<<(std::ostream& out, const Progression& progression);

    virtual ~Progression();

protected:
    double* _progressionMembs = nullptr;
    double _firstElement = 0;
    double _feature = 0;
    double _sum = 0;
    bool _sumCalculated = false;
    bool _featurePassed = false;
    int _elementsAmount = 0;

    static bool DbsAreSame(double a, double b);

    void CleanUp();
};


#endif LAB2_OOP_PROGRESSION_H
