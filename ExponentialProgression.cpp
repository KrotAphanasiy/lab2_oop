//
// Created by michael on 15.09.2020.
//

#include "ExponentialProgression.h"
#include <cmath>


ExponentialProgression::ExponentialProgression(double *progressionMembs, int elementsAmount) :
        Progression(progressionMembs, elementsAmount),
        _secondElement(_progressionMembs[1]){

    double prev = _progressionMembs[1] / _progressionMembs[0];

    for (int counter = 2; counter < _elementsAmount; counter++){
        if(!DbsAreSame(_progressionMembs[counter] / _progressionMembs[counter - 1], prev)) {
            CleanUp();
            throw ProgressionExc("Sequence is not progression");
        }
    }

    _feature = _secondElement / _firstElement;
    _featurePassed = true;
}

ExponentialProgression::ExponentialProgression(double firstElement, int elementsAmount, double feature) :
    Progression(firstElement, elementsAmount, feature) {

}

ExponentialProgression::ExponentialProgression(double firstElement, double secondElement, int elementsAmount):
    Progression(firstElement, elementsAmount),
    _secondElement(secondElement){
}


double ExponentialProgression::CalculateSum() {
    if(!_sumCalculated) {
        if(!_featurePassed){
            CalculateFeature();
        }
        _sum = _firstElement * (1 - pow(_feature, _elementsAmount));
        _sum = _sum / (1 - _feature);
        _sumCalculated = true;
    }
    return _sum;
}

double ExponentialProgression::CalculateAbsoluteSum() {
    if(std::abs(_feature) < 1){
        if(!_infDecrSumCalculated) {
            _infDecrSum = _firstElement / (1 - _feature);
            _infDecrSumCalculated = true;
        }
        return _infDecrSum;
    }else{
        throw ProgressionExc("unable to calculate");
    }
}

double ExponentialProgression::CalculateNElement(int elNumber) {
    CalculateFeature();
    return _firstElement * pow(_feature, elNumber-1);
}

double ExponentialProgression::CalculateFeature() {
    if(!_featurePassed) {
        _feature = _secondElement / _firstElement;
        _featurePassed = true;
    }
    return _feature;
}
